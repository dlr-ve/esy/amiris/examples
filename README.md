[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.7789049.svg)](https://doi.org/10.5281/zenodo.7789049)

# Examples
Example configurations built for [AMIRIS 3.4.0](https://gitlab.com/dlr-ve/esy/amiris/amiris/-/releases/v3.4.0)
See the individual licence information in the respective scenarios.

## How-to
Please use [AMIRIS-Py](https://gitlab.com/dlr-ve/esy/amiris/amiris-py) to run AMIRIS-Examples.
Follow these very few instructions in its [README](https://gitlab.com/dlr-ve/esy/amiris/amiris-py/-/blob/main/README.md) to have AMIRIS running in no time.

## Sample scenarios
* [Simple](https://gitlab.com/dlr-ve/esy/amiris/examples/-/tree/main/Simple): simple scenario running for one day with dummy data
* [SimpleCoupled](https://gitlab.com/dlr-ve/esy/amiris/examples/-/tree/main/SimpleCoupled): simple scenario coupling two markets running for one day with dummy data
* [Austria2019](https://gitlab.com/dlr-ve/esy/amiris/examples/-/tree/main/Austria2019): historical simulation of Austrian day-ahead market in 2019
* [Germany2015](https://gitlab.com/dlr-ve/esy/amiris/examples/-/tree/main/Germany2015): historical simulation of German day-ahead market in 2015
* [Germany2016](https://gitlab.com/dlr-ve/esy/amiris/examples/-/tree/main/Germany2016): historical simulation of German day-ahead market in 2016
* [Germany2017](https://gitlab.com/dlr-ve/esy/amiris/examples/-/tree/main/Germany2017): historical simulation of German day-ahead market in 2017
* [Germany2018](https://gitlab.com/dlr-ve/esy/amiris/examples/-/tree/main/Germany2018): historical simulation of German day-ahead market in 2018
* [Germany2019](https://gitlab.com/dlr-ve/esy/amiris/examples/-/tree/main/Germany2019): historical simulation of German day-ahead market in 2019

## Citing AMIRIS Examples
If you use AMIRIS Examples in your scientific work please cite:

Kristina Nienhaus, Christoph Schimeczek, Ulrich Frey, Evelyn Sperber, Seyedfarzad Sarfarazi, Felix Nitsch, Johannes Kochems & A. Achraf El Ghazi (2023). AMIRIS Examples. Zenodo. [doi: 10.5281/zenodo.7789049](https://doi.org/10.5281/zenodo.7789049)

## Acknowledgments
We thank those who contributed to enhancing the AMIRIS examples:
* [v1.1](https://gitlab.com/dlr-ve/esy/amiris/examples/-/releases/v1.1) Dr. Tom Bauermann, for pointing out errors in historical fuel prices
