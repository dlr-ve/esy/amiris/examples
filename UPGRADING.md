<!-- SPDX-FileCopyrightText: 2023 German Aerospace Center <amiris@dlr.de>

SPDX-License-Identifier: Apache-2.0 -->
# Upgrading

## [3.0.0]
This version requires AMIRIS version >= v3.0.0.
We recommend to use [amirispy]() and download the latest AMIRIS artifact like so: `amiris install --force --mode model`.

## [2.0.0]
This version requires AMIRIS version >= v2.0.0.
We recommend to use [amirispy]() and download the latest AMIRIS artifact like so: `amiris install --force --mode model`.