Schema: !include "schema.yaml"

GeneralProperties:
  RunId: 1
  Simulation:
    StartTime: 2015-12-31_23:58:00
    StopTime: 2016-12-30_23:58:00
    RandomSeed: 1

StringSets:
  FuelType:
    Values: 
      OIL:
        MetaData:
          Description: "Fuel type oil"
          OEONearestConcept: OEO_00010316
      HARD_COAL:
        MetaData:
          Description: "Fuel type hard coal"
          OEONearestConcept: OEO_00000204
      LIGNITE:
        MetaData:
          Description: "Fuel type lignite"
          OEONearestConcept: OEO_00000251
      NUCLEAR:
        MetaData:
          Description: "Fuel type nuclear"
          OEONearestConcept: OEO_00000302
      WASTE:
        MetaData:
          Description: "Fuel type waste"
          OEONearestConcept: OEO_00000439
      NATURAL_GAS:
        MetaData:
          Description: "Fuel type natural gas"
          OEONearestConcept: OEO_00000292
      HYDROGEN:
        MetaData:
          Description: "Fuel type hydrogen gas"
      BIOMASS:
        MetaData:
          Description: "Fuel type biogas or other bio-fuels"
      OTHER:
        MetaData:
          Description: "Any other type of fuel"
  PolicySet:
    Values: ['PVRooftop', 'WindOn', 'WindOff', 'RunOfRiver', 'OtherPV', 'Biogas', 'Undefined', 'PvFit', 'PvMpvarCluster1', 'PvMpvarCluster2', 'PvMpvarCluster3', 'PvMpvarCluster4', 'PvMpvarCluster5', 'WindOnFit', 'WindOnMpvarCluster1', 'WindOnMpvarCluster2', 'WindOnMpvarCluster3', 'WindOnMpvarCluster4', 'WindOnMpvarCluster5', 'WindOffMpvarCluster1', 'WindOffMpvarCluster2', 'WindOffMpvarCluster3', 'WindOffMpvarCluster4',]

Variables:
  - &portfolioBuildingOffset 60

Agents:
  - Type: DayAheadMarketSingleZone
    Id: 1
    Attributes:
      Clearing: &clearingParameters
        DistributionMethod: SAME_SHARES
      GateClosureInfoOffsetInSeconds: 31

  - Type: CarbonMarket
    Id: 3
    Attributes:
      OperationMode: FIXED
      Co2Prices: "./timeseries/co2_price.csv"

  - Type: FuelsMarket
    Id: 4
    Attributes:
      FuelPrices:
        - FuelType: NUCLEAR
          Price: 2.00
          ConversionFactor: 1.0
        - FuelType: LIGNITE
          Price: 5.00
          ConversionFactor: 1.0
        - FuelType: HARD_COAL
          Price: "./timeseries/hard_coal_price.csv"
          ConversionFactor: 1.0
        - FuelType: NATURAL_GAS
          Price: "./timeseries/natural_gas_price.csv"
          ConversionFactor: 1.0
        - FuelType: OIL
          Price: "./timeseries/oil_price.csv"
          ConversionFactor: 1.0

  - Type: DemandTrader
    Id: 100
    Attributes:
      Loads:
        - ValueOfLostLoad: 3000.0
          DemandSeries: "./timeseries/load.csv"

  - Type: MeritOrderForecaster
    Id: 6
    Attributes:
      Clearing: *clearingParameters
      ForecastPeriodInHours: 168

  - Type: StorageTrader #Pumped Hydro
    Id: 7
    Attributes:
      Device:
        EnergyToPowerRatio: 5.0
        SelfDischargeRatePerHour: 0.0
        ChargingEfficiency: 0.89
        DischargingEfficiency: 0.89
        InitialEnergyLevelInMWH: 1000.
        InstalledPowerInMW: 8000.
      Strategy:
        StrategistType: SINGLE_AGENT_MIN_SYSTEM_COST
        ForecastPeriodInHours: 168
        ScheduleDurationInHours: 24
        SingleAgent:
          ModelledChargingSteps: 50

  - Type: RenewableTrader
    Id: 11
    Attributes:
      ShareOfRevenues: 0.0

  - Type: NoSupportTrader
    Id: 12
    Attributes:
      ShareOfRevenues: 0.0

  - Type: SystemOperatorTrader
    Id: 13


  - Type: SupportPolicy
    Id: 90
    Attributes:
      SetSupportData:
        - PolicySet: PvFit
          FIT:
            TsFit: 120.0
        - PolicySet: WindOnFit
          FIT:
            TsFit: 85.0
        - PolicySet: RunOfRiver
          FIT:
            TsFit: 100.0
        - PolicySet: PvMpvarCluster1
          MPVAR:
            Lcoe: 101.02
        - PolicySet: PvMpvarCluster2
          MPVAR:
            Lcoe: 183.4
        - PolicySet: PvMpvarCluster3
          MPVAR:
            Lcoe: 224.8
        - PolicySet: PvMpvarCluster4
          MPVAR:
            Lcoe: 286.67
        - PolicySet: PvMpvarCluster5
          MPVAR:
            Lcoe: 329.45
        - PolicySet: WindOnMpvarCluster1
          MPVAR:
            Lcoe: 85.79
        - PolicySet: WindOnMpvarCluster2
          MPVAR:
            Lcoe: 89
        - PolicySet: WindOnMpvarCluster3
          MPVAR:
            Lcoe: 95.91
        - PolicySet: WindOnMpvarCluster4
          MPVAR:
            Lcoe: 98.62
        - PolicySet: WindOnMpvarCluster5
          MPVAR:
            Lcoe: 100.84
        - PolicySet: WindOffMpvarCluster1
          MPVAR:
            Lcoe: 154
        - PolicySet: WindOffMpvarCluster2
          MPVAR:
            Lcoe: 174.5
        - PolicySet: WindOffMpvarCluster3
          MPVAR:
            Lcoe: 194
        - PolicySet: WindOffMpvarCluster4
          MPVAR:
            Lcoe: 194

  - Type: ConventionalPlantOperator
    Id: 500

  - Type: PredefinedPlantBuilder
    Id: 2000
    Attributes:
      PortfolioBuildingOffsetInSeconds: *portfolioBuildingOffset
      Prototype:
        FuelType: NUCLEAR
        SpecificCo2EmissionsInTperMWH: 0.0
        PlannedAvailability: "./timeseries/nuclear_availability.csv"
        UnplannedAvailabilityFactor: 1.
        OpexVarInEURperMWH: 0.5
        CyclingCostInEURperMW: 0.0
      Efficiency:
        Minimal: 0.330
        Maximal: 0.331
      BlockSizeInMW: 900.0
      InstalledPowerInMW: 10799.0

  - Type: ConventionalTrader
    Id: 1000
    Attributes:
      minMarkup: -150
      maxMarkup: -90

  - Type: ConventionalPlantOperator
    Id: 501

  - Type: PredefinedPlantBuilder
    Id: 2001
    Attributes:
      PortfolioBuildingOffsetInSeconds: *portfolioBuildingOffset
      Prototype:
        FuelType: LIGNITE
        SpecificCo2EmissionsInTperMWH: 0.364
        PlannedAvailability: "./timeseries/lignite_availability.csv"
        UnplannedAvailabilityFactor: 0.902
        OpexVarInEURperMWH: 2.0
        CyclingCostInEURperMW: 0.0
      Efficiency:
        Minimal: 0.3108
        Maximal: 0.45
      BlockSizeInMW: 500.0
      InstalledPowerInMW: 21262.

  - Type: ConventionalTrader
    Id: 1001
    Attributes:
      minMarkup: -70
      maxMarkup: -5

  - Type: ConventionalPlantOperator
    Id: 502

  - Type: PredefinedPlantBuilder
    Id: 2002
    Attributes:
      PortfolioBuildingOffsetInSeconds: *portfolioBuildingOffset
      Prototype:
        FuelType: HARD_COAL
        SpecificCo2EmissionsInTperMWH: 0.341
        PlannedAvailability: "./timeseries/hard_coal_availability.csv"
        UnplannedAvailabilityFactor: 0.699
        OpexVarInEURperMWH: 2.5
        CyclingCostInEURperMW: 0.0
      Efficiency:
        Minimal: 0.3317
        Maximal: 0.4893
      BlockSizeInMW: 300.0
      InstalledPowerInMW: 28035.

  - Type: ConventionalTrader
    Id: 1002
    Attributes:
      minMarkup: -12
      maxMarkup: 12

  - Type: ConventionalPlantOperator
    Id: 503

  - Type: PredefinedPlantBuilder
    Id: 2003
    Attributes:
      PortfolioBuildingOffsetInSeconds: *portfolioBuildingOffset
      Prototype:
        FuelType: NATURAL_GAS #gasCC
        SpecificCo2EmissionsInTperMWH: 0.201
        PlannedAvailability: 1.0
        UnplannedAvailabilityFactor: 0.97
        OpexVarInEURperMWH: 1.2
        CyclingCostInEURperMW: 0
      Efficiency:
        Minimal: 0.5158
        Maximal: 0.6166
      BlockSizeInMW: 200.0
      InstalledPowerInMW: 13623.829

  - Type: ConventionalTrader
    Id: 1003
    Attributes:
      minMarkup: -20
      maxMarkup: 5

  - Type: ConventionalPlantOperator
    Id: 504

  - Type: PredefinedPlantBuilder
    Id: 2004
    Attributes:
      PortfolioBuildingOffsetInSeconds: *portfolioBuildingOffset
      Prototype:
        FuelType: NATURAL_GAS #gasTurbine
        SpecificCo2EmissionsInTperMWH: 0.201
        PlannedAvailability: 1.0
        UnplannedAvailabilityFactor: 0.97
        OpexVarInEURperMWH: 1.2
        CyclingCostInEURperMW: 0.0
      Efficiency:
        Minimal: 0.3174
        Maximal: 0.4491
      BlockSizeInMW: 100.0
      InstalledPowerInMW: 13180.7515

  - Type: ConventionalTrader
    Id: 1004
    Attributes:
      minMarkup: -15
      maxMarkup: 15

  - Type: ConventionalPlantOperator
    Id: 505

  - Type: PredefinedPlantBuilder
    Id: 2005
    Attributes:
      PortfolioBuildingOffsetInSeconds: *portfolioBuildingOffset
      Prototype:
        FuelType: OIL # oil, other fossil fuels, mixed fossil fuels
        SpecificCo2EmissionsInTperMWH: 0.264
        PlannedAvailability: 1.0
        UnplannedAvailabilityFactor: 0.93
        OpexVarInEURperMWH: 1.2
        CyclingCostInEURperMW: 0.0
      Efficiency:
        Minimal: 0.304
        Maximal: 0.3982
      BlockSizeInMW: 100.0
      InstalledPowerInMW: 4338.1

  - Type: ConventionalTrader
    Id: 1005
    Attributes:
      minMarkup: 10
      maxMarkup: 20

  - Type: VariableRenewableOperator
    Id: 10
    Attributes:
      PolicySet: PvFit
      EnergyCarrier: PV
      SupportInstrument: FIT
      InstalledPowerInMW: 32332.
      OpexVarInEURperMWH: 0.0
      YieldProfile: "./timeseries/solar_profile.csv"

  - Type: VariableRenewableOperator
    Id: 20
    Attributes:
      PolicySet: WindOnFit
      SupportInstrument: FIT
      EnergyCarrier: WindOn
      InstalledPowerInMW: 27131.
      OpexVarInEURperMWH: 0.0
      YieldProfile: "./timeseries/wind_onshore_profile.csv"

  - Type: VariableRenewableOperator
    Id: 50
    Attributes:
      PolicySet: RunOfRiver
      SupportInstrument: FIT
      EnergyCarrier: RunOfRiver
      InstalledPowerInMW: 14028.
      OpexVarInEURperMWH: 0.0
      YieldProfile: "./timeseries/run_of_river_profile.csv"

  - Type: Biogas
    Id: 52
    Attributes:
      PolicySet: Biogas
      EnergyCarrier: Biogas
      InstalledPowerInMW: 7556.
      OpexVarInEURperMWH: 0.0
      DispatchTimeSeries: "./timeseries/biomass_profile.csv"
      OperationMode: FROM_FILE

  - Type: VariableRenewableOperator
    Id: 53
    Attributes:
      EnergyCarrier: Other #otherres
      InstalledPowerInMW: 583.
      OpexVarInEURperMWH: 1.2
      YieldProfile: "./timeseries/other_res_profile.csv"

  - Type: VariableRenewableOperator
    Id: 60
    Attributes:
      PolicySet: PvMpvarCluster1
      EnergyCarrier: PV
      SupportInstrument: MPVAR
      InstalledPowerInMW: 3386.
      OpexVarInEURperMWH: 0.0
      YieldProfile: "./timeseries/solar_profile.csv"

  - Type: VariableRenewableOperator
    Id: 61
    Attributes:
      PolicySet: PvMpvarCluster2
      EnergyCarrier: PV
      SupportInstrument: MPVAR
      InstalledPowerInMW: 2484.
      OpexVarInEURperMWH: 0.0
      YieldProfile: "./timeseries/solar_profile.csv"

  - Type: VariableRenewableOperator
    Id: 62
    Attributes:
      PolicySet: PvMpvarCluster3
      EnergyCarrier: PV
      SupportInstrument: MPVAR
      InstalledPowerInMW: 2200.
      OpexVarInEURperMWH: 0.0
      YieldProfile: "./timeseries/solar_profile.csv"

  - Type: VariableRenewableOperator
    Id: 63
    Attributes:
      PolicySet: PvMpvarCluster4
      EnergyCarrier: PV
      SupportInstrument: MPVAR
      InstalledPowerInMW: 1356.
      OpexVarInEURperMWH: 0.0
      YieldProfile: "./timeseries/solar_profile.csv"

  - Type: VariableRenewableOperator
    Id: 64
    Attributes:
      PolicySet: PvMpvarCluster5
      EnergyCarrier: PV
      SupportInstrument: MPVAR
      InstalledPowerInMW: 108.
      OpexVarInEURperMWH: 0.0
      YieldProfile: "./timeseries/solar_profile.csv"

  - Type: VariableRenewableOperator
    Id: 70
    Attributes:
      PolicySet: WindOnMpvarCluster1
      SupportInstrument: MPVAR
      EnergyCarrier: WindOn
      InstalledPowerInMW: 4719.
      OpexVarInEURperMWH: 0.0
      YieldProfile: "./timeseries/wind_onshore_profile.csv"

  - Type: VariableRenewableOperator
    Id: 71
    Attributes:
      PolicySet: WindOnMpvarCluster2
      SupportInstrument: MPVAR
      EnergyCarrier: WindOn
      InstalledPowerInMW: 4023.
      OpexVarInEURperMWH: 0.0
      YieldProfile: "./timeseries/wind_onshore_profile.csv"

  - Type: VariableRenewableOperator
    Id: 72
    Attributes:
      PolicySet: WindOnMpvarCluster3
      SupportInstrument: MPVAR
      EnergyCarrier: WindOn
      InstalledPowerInMW: 3290.
      OpexVarInEURperMWH: 0.0
      YieldProfile: "./timeseries/wind_onshore_profile.csv"

  - Type: VariableRenewableOperator
    Id: 73
    Attributes:
      PolicySet: WindOnMpvarCluster4
      SupportInstrument: MPVAR
      EnergyCarrier: WindOn
      InstalledPowerInMW: 3724.
      OpexVarInEURperMWH: 0.0
      YieldProfile: "./timeseries/wind_onshore_profile.csv"

  - Type: VariableRenewableOperator
    Id: 74
    Attributes:
      PolicySet: WindOnMpvarCluster5
      SupportInstrument: MPVAR
      EnergyCarrier: WindOn
      InstalledPowerInMW: 6975.
      OpexVarInEURperMWH: 0.0
      YieldProfile: "./timeseries/wind_onshore_profile.csv"

  - Type: VariableRenewableOperator
    Id: 80
    Attributes:
      PolicySet: WindOffMpvarCluster1
      SupportInstrument: MPVAR
      EnergyCarrier: WindOff
      InstalledPowerInMW: 642.
      OpexVarInEURperMWH: 0.0
      YieldProfile: "./timeseries/wind_offshore_profile.csv"

  - Type: VariableRenewableOperator
    Id: 81
    Attributes:
      PolicySet: WindOffMpvarCluster2
      SupportInstrument: MPVAR
      EnergyCarrier: WindOff
      InstalledPowerInMW: 80.
      OpexVarInEURperMWH: 0.0
      YieldProfile: "./timeseries/wind_offshore_profile.csv"

  - Type: VariableRenewableOperator
    Id: 82
    Attributes:
      PolicySet: WindOffMpvarCluster3
      SupportInstrument: MPVAR
      EnergyCarrier: WindOff
      InstalledPowerInMW: 3409.
      OpexVarInEURperMWH: 0.0
      YieldProfile: "./timeseries/wind_offshore_profile.csv"

  - Type: VariableRenewableOperator
    Id: 83
    Attributes:
      PolicySet: WindOffMpvarCluster4
      SupportInstrument: MPVAR
      EnergyCarrier: WindOff
      InstalledPowerInMW: 0.
      OpexVarInEURperMWH: 0.0
      YieldProfile: "./timeseries/wind_offshore_profile.csv"

Contracts: !include ["contracts/*.yaml", "Contracts"]
