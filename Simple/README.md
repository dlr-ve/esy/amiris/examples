# Simple Example
Simple scenario running for one day with dummy data

## Time Series
| File                      | Description                                            | Unit        |
|---------------------------|--------------------------------------------------------|-------------|
| `load.csv`                | Hourly electric energy demand                          | MWh/h       |
| `pv_yield_profile.csv`    | Normalized profile of generation for PV power plants   | 1           |
| `wind_yield_profile.csv`  | Normalized profile of generation for wind power plants | 1           |
